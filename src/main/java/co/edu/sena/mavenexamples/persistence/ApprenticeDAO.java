/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.mavenexamples.persistence;

import co.edu.sena.mavenexamples.model.Apprentice;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Jaider osorio
 */
public class ApprenticeDAO implements IApprtenticeDAO{

    @Override
    public Apprentice findByid(Long document) throws Exception {
            try {
            return EntityManagerHelper.getEntityManager().find(Apprentice.class, document);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Apprentice> findAll() throws Exception {
              try {
                  Query query = EntityManagerHelper.getEntityManager().createNamedQuery("Apprentice.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
