/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.mavenexamples.controller;

import co.edu.sena.mavenexamples.model.Apprentice;
import co.edu.sena.mavenexamples.persistence.DAOFactory;
import co.edu.sena.mavenexamples.view.utils.Constants;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Jaider osorio
 */
public class ApprenticeController {

    public Apprentice findById(Long document) throws Exception {
        if (document == 0) {
            throw new Exception("el documento es obligatorio");

        }
        return DAOFactory.getApprtenticeDAO().findByid(document);
    }

    public List<Apprentice> findAll() throws Exception {
        return DAOFactory.getApprtenticeDAO().findAll();
    }

    public String setMesage() throws Exception {
        String message = "";
        try {
            List<Apprentice> apprentices = findAll();
            message = "<p>Hola querido jaider osorio hoy es un gran dia </p>"
                    + "<table border='1'>"
                    + "<tr><th>Documento</th><th>Nombre</th><th>Programa</th></tr>";
            for (Apprentice apprentice : apprentices) {
                message += "<tr>" + "<td>" + apprentice.getDocument() + "</td>"
                        + "<td>" + apprentice.getFullName() + "</td>"
                        + "<td>" + apprentice.getIdCourse().getCareer() + "</td>"
                        + "</tr>";

            }
            message += "</table>";
        } catch (Exception e) {
            throw e;
        }
        return message;

    }

    public void sedEmail(String to, String subjet) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable","true");

        /*javax.mail*/
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        String bodyMessage = setMesage();
        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from);
        message.setSubject(subjet);
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        message.setContent(bodyMessage, "text/html; charset=utf-8");
        /*enviar mensaje */

        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

}
