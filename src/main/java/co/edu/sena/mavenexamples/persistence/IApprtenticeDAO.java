/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.mavenexamples.persistence;

import co.edu.sena.mavenexamples.model.Apprentice;
import java.util.List;

/**
 *
 * @author Jaider osorio
 */
public interface IApprtenticeDAO {
  public Apprentice findByid(Long document)throws Exception;
  public List<Apprentice> findAll() throws Exception;
}
